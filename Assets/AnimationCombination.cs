﻿using UnityEngine;
using System.Collections;
using System;
using DG.Tweening;

public class AnimationCombination : MonoBehaviour {
    private AnimationManager animationManager;
    private int valueCombine;
    private int value2;
    private int value1;
    FrameContentData auxContentData = new FrameContentData();
    private bool NoAnim;

    // Use this for initialization
    void Start () {
	    animationManager = GetComponent<AnimationManager>();
        animationManager.AnimationPlayCustom.Add(CombinationAnimation);
        animationManager.AnimationFirstFrameCustom.Add(FirstFrame);
	}

    private bool FirstFrame(AnimationConfiguration arg) {
        Debug.Log("FIRST FRAME TRY");
        NoAnim = true;
        var qs = arg.QuantityConcepts;
        var frameCs = arg.FrameContents;
        value1 = -1;
        value2 = -1;
        valueCombine = 0;
        for (int i = 0; i < qs.Count; i++) {
            if(qs[i].Type != QuantityConcept.QuantityTypeEnum.EXIST && 
                qs[i].Type != QuantityConcept.QuantityTypeEnum.COMBINE
                ) {
                Debug.Log("Give up, not combine story");
                return false;
            }
            
        }
        for (int i = 0; i < qs.Count; i++) {
            if(qs[i].Type == QuantityConcept.QuantityTypeEnum.EXIST) {
                //if(frameCs[i].BackgroundEnum == B)
                if(value1 == -1) {
                    value1 = qs[i].Number;
                } else {
                    if(value2 == -1) {
                        value2 = qs[i].Number;
                    }
                }
            }
            if(qs[i].Type == QuantityConcept.QuantityTypeEnum.COMBINE) {
                valueCombine = qs[i].SumOfNumbers();
            }
        }
        Debug.Log("Before checking background enums");
        if(frameCs[0].BackgroundEnum == BackgroundsEnum.backgroundtableboy || 
            frameCs[0].BackgroundEnum == BackgroundsEnum.backgroundtablegirl ||
            frameCs[0].BackgroundEnum == BackgroundsEnum.backgroundtableboth
            ) {
            var imageFrame = animationManager.ImageFrame;
            auxContentData.BackgroundEnum =  BackgroundsEnum.backgroundtablebothanim;
            auxContentData.ClearObjects();
            for (int i = 0; i < frameCs.Count; i++) {
                if(frameCs[i].BackgroundEnum == BackgroundsEnum.backgroundtableboy) {
                    auxContentData.AddObjects(frameCs[i].ObjectsEnums[0], frameCs[i].ObjectsEnums.Count, 0);
                    
                }
                if(frameCs[i].BackgroundEnum == BackgroundsEnum.backgroundtablegirl) {
                    auxContentData.AddObjects(frameCs[i].ObjectsEnums[0], frameCs[i].ObjectsEnums.Count, 1);
                }
            }
            
            animationManager.FramesManager.ContentToFrame(imageFrame, auxContentData);
            Debug.Log("CONTENT FRAME SUCESS");
        }
        NoAnim = false;
        Debug.Log("FIRST FRAME SUCESS");
        return true;
    }

    private bool CombinationAnimation(AnimationConfiguration arg) {
        if(NoAnim) {
            return false;
        }
        var qs = arg.QuantityConcepts;
        var frameCs = arg.FrameContents;

        if(frameCs[0].BackgroundEnum == BackgroundsEnum.backgroundtableboy || 
            frameCs[0].BackgroundEnum == BackgroundsEnum.backgroundtablegirl
            ) {
            TableCombination(arg, value1, value2, valueCombine);
        }
        
        return true;
    }

    private void TableCombination(AnimationConfiguration arg, int value1, int value2, int valueCombine) {
        
        var imageFrame = animationManager.ImageFrame;
        
        GameObject boy = imageFrame.GetCustomObject(0);
        GameObject girl = imageFrame.GetCustomObject(1);
        GameObject boyStuff = imageFrame.GetCustomObject(2);
        GameObject girlStuff = imageFrame.GetCustomObject(3);
        
        var seq = DOTween.Sequence();
        //boy move
        seq.Append(boy.transform.DOMove(imageFrame.GetCustomPosition(0), 0.3f));
        seq.AppendInterval(0.3f);
        seq.Append(boy.transform.DOMove(imageFrame.GetCustomPosition(2), 0.3f));
        //girl move
        seq.Append(girl.transform.DOMove(imageFrame.GetCustomPosition(1), 0.3f));
        seq.AppendInterval(0.3f);
        seq.Append(girl.transform.DOMove(imageFrame.GetCustomPosition(3), 0.3f));
        //both move
        seq.Append(boy.transform.DOMove(imageFrame.GetCustomPosition(0), 0.3f));
        seq.Append(girl.transform.DOMove(imageFrame.GetCustomPosition(1), 0.3f));
        var objectsArrays = imageFrame.ObjectsInPosition;
        int targetPosIndex = 0;
        for (int i = 0; i < objectsArrays.Length; i++) {
            var objectArray = objectsArrays[i];
            for (int j = 0; j < objectArray.Count; j++) {
                var obj = objectArray[j];
                seq.Append(obj.transform.DOMove(imageFrame.GetObjectPositionOutside(targetPosIndex), 0.3f));
                targetPosIndex++;
            }
        }

        //seq.Append(boyStuff.transform.DOMove(imageFrame.GetCustomPosition(4), 0.3f));
        //seq.Append(girlStuff.transform.DOMove(imageFrame.GetCustomPosition(5), 0.3f));
        seq.AppendInterval(0.3f);
        seq.AppendCallback(animationManager.AnimationOver);
        
        
    }

    // Update is called once per frame
    void Update () {
	
	}
}
