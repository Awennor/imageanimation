﻿using UnityEngine;
using System.Collections;

public class PhraseQuantitySetup : MonoBehaviour {

    public ConceptHolder conceptHolder;
    public CardToAnimationManager cardToAnimationArranger;

    // Use this for initialization
    public void StartSetup() {
        for (int i = 0; i < 3; i++) {
            var q = cardToAnimationArranger.QuantityConceptOfFrameOnDragDrop(i);
            var content = cardToAnimationArranger.ContentOfFrameOnDragDrop(i);
            var objectsEnum = content.ObjectsEnums[0];
            var type = q.Type;
            var number = q.Number;
            for (int j = 0; j < 2; j++) {
                
                if(j == 1 && i == 2){
                    number += 1;
                }
                if(j == 1 && i == 1){
                    objectsEnum = ObjectsEnum.apple;
                }
                if(j == 1 && i == 0){
                    type = QuantityConcept.QuantityTypeEnum.INCREASE;
                }
                    
                string objectName = "とり";
                if (objectsEnum == ObjectsEnum.apple) {
                    objectName = "りんご";
                }

                
                if (type == QuantityConcept.QuantityTypeEnum.EXIST)
                    conceptHolder.SetText(i + j * 3, objectName + "が" + number + "あります");
                if (type == QuantityConcept.QuantityTypeEnum.DECREASE)
                    conceptHolder.SetText(i + j * 3, objectName + "が" + number + "でっています");
                if (type == QuantityConcept.QuantityTypeEnum.INCREASE)
                    conceptHolder.SetText(i + j * 3, objectName + "が" + number + "きます");
            }


        }
    }

    // Update is called once per frame
    void Update() {

    }
}
