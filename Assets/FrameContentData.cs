﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[Serializable]
public class FrameContentData {
    

    [SerializeField]
    List<ObjectsEnum> objectsEnums = new List<ObjectsEnum>();
    [SerializeField]
    List<int> objectsEnumGroupId = new List<int>();
    [SerializeField]
    List<ObjectGraphicalRepresentations>  representationType = new List<ObjectGraphicalRepresentations>();
    [SerializeField]
    BackgroundsEnum backgroundEnum;



    public List<ObjectsEnum> ObjectsEnums {
        get {
            return objectsEnums;
        }

        set {
            objectsEnums = value;
        }
    }

    public void FillObjects(ObjectsEnum bird, int amount) {
        FillObjects(bird, amount, 0);

    }

    public void FillObjects(ObjectsEnum bird, int amount, int groupId) {
        ClearObjects();
        AddObjects(bird,amount, groupId);
    }

    public void FillObjects(ObjectsEnum bird, int amount, int groupId, ObjectGraphicalRepresentations represen) {
        ClearObjects();
        AddObjects(bird,amount, groupId, represen);
    }

    public  void AddObjects(ObjectsEnum bird, int amount, int groupId) {
        var Represen = ObjectGraphicalRepresentations.STATIC;
        AddObjects(bird, amount, groupId, Represen);

    }

    public void AddObjects(ObjectsEnum bird, int amount, int groupId, ObjectGraphicalRepresentations Represen) {
        for (int i = 0; i < amount; i++) {
            objectsEnums.Add(bird);
            ObjectsEnumGroupId.Add(groupId);
            representationType.Add(Represen);
        }
    }

    public List<ObjectGraphicalRepresentations> RepresentationType {
        get {
            return representationType;
        }

        set {
            representationType = value;
        }
    }



    public BackgroundsEnum BackgroundEnum {
        get {
            return backgroundEnum;
        }

        set {
            backgroundEnum = value;
        }
    }

    public List<int> ObjectsEnumGroupId {
        get {
            //while(objectsEnumGroupId.Count < objectsEnums.Count)
               //objectsEnumGroupId.Add(0);
            return objectsEnumGroupId;
        }

        set {
            objectsEnumGroupId = value;
        }
    }

    internal void ClearObjects() {
        objectsEnums.Clear();
        objectsEnumGroupId.Clear();
        representationType.Clear();
    }
}
