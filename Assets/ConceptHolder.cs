﻿using UnityEngine;
using System.Collections;
using System;

public class ConceptHolder : MonoBehaviour {

    public AutomaticPositioner automaticPositioner;
    [SerializeField]
    ConceptCard[] conceptCards;

	// Use this for initialization
	void Start () {
        for (int i = 0; i < conceptCards.Length; i++)
        {
            automaticPositioner.Objects.Add(conceptCards[i].gameObject);
        }
        
	}

    public void Reposition() {
        automaticPositioner.RandomReposition();

    }

	
	// Update is called once per frame
	void Update () {
	    
	}

    internal void SetText(int v1, string v2) {
        conceptCards[v1].Text.text = v2;
    }
}
