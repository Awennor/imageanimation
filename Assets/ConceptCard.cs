﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ConceptCard : MonoBehaviour {

    [SerializeField]
    private Text text;

    public Text Text {
        get {
            return text;
        }

        set {
            text = value;
        }
    }


    // Use this for initialization
    void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
