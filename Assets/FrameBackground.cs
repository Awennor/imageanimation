﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class GameObjectArray {
    public GameObject[] array;

    public GameObject this[int index]    // Indexer declaration
    {
        get {
            return array[index];
        }

        set {
            array[index] = value;
        }
        
    }

}

public class FrameBackground : MonoBehaviour {

    public GameObject graphics;
    [SerializeField]
    private GameObjectArray[] objectPositions;
    [SerializeField]
    private GameObject[] outsidePositions;

    [SerializeField]
    private GameObject[] extraObjects;
    [SerializeField]
    private GameObject[] extraPositions;

    public GameObjectArray[] ObjectPositions {
        get {
            return objectPositions;
        }

        set {
            objectPositions = value;
        }
    }

    public GameObject[] ObjectPositionsGroupZero {
        get {
            return ObjectPositions[0].array;
        }

        set {
            ObjectPositions[0].array = value;
        }
    }

    public GameObject[] OutsidePositions {
        get {
            return outsidePositions;
        }

        set {
            outsidePositions = value;
        }
    }

    public GameObject[] ExtraObjects {
        get {
            return extraObjects;
        }

        set {
            extraObjects = value;
        }
    }

    public GameObject[] ExtraPositions {
        get {
            return extraPositions;
        }

        set {
            extraPositions = value;
        }
    }

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }
}
