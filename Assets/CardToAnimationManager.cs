﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using System;

public class CardToAnimationManager : MonoBehaviour {

    public FramesManager framesManager;
    public DragAndDropManager dragAndDropManager;
    public AnimationManager animationManager;
    public UnityEvent wrongAnimation;

    [SerializeField]
    List<QuantityConcept> quantityConceptsOfFrames = new List<QuantityConcept>();
    private bool increaseProblem;

    public List<Func<bool>> AnimationVerificationCustom = new List<Func<bool>>();

    internal void HideUnused()
    {
        framesManager.HideAllFrames();
        for (int i = 0; i < 3; i++)
        {
            framesManager.ShowFrame(dragAndDropManager.FrameIndexOnSpot(i));
        }
        
    }

    public List<QuantityConcept> QuantityConceptsOfFrames {
        get {
            return quantityConceptsOfFrames;
        }

        set {
            quantityConceptsOfFrames = value;
        }
    }

    public bool IncreaseProblem
    {
        get
        {
            return increaseProblem;
        }

        set
        {
            increaseProblem = value;
        }
    }

    internal QuantityConcept QuantityConceptOfFrameOnDragDrop(int i) {
        int frameId = dragAndDropManager.FrameIndexOnSpot(i);
        return quantityConceptsOfFrames[frameId];
    }

    internal FrameContentData ContentOfFrameOnDragDrop(int i) {
        int frameId = dragAndDropManager.FrameIndexOnSpot(i);
        return framesManager.FrameContentDatas[frameId];
    }


    // Use this for initialization
    void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void ButtonPress() {
        int exist = -1; 
        bool pass = true;
        bool customVerification = false;

        for (int i = 0; i < AnimationVerificationCustom.Count; i++) {
            
            bool interrupt = AnimationVerificationCustom[i]();
            if(interrupt) customVerification = true;
        }

        IncreaseProblem = false;
        if(customVerification == false) {
            for (int i = 0; i < dragAndDropManager.DropSpaceAmount; i++) {
            int frameIndex = dragAndDropManager.FrameIndexOnSpot(i);
            if(frameIndex  == -1) {
                pass = false;
                break;
            }
            var qC = QuantityConceptsOfFrames[frameIndex];
            if(qC.Type == QuantityConcept.QuantityTypeEnum.EXIST) {
                Debug.Log("Exist Card");
                if(exist == -1 || exist == qC.Number) {
                    Debug.Log("Exist Card No Prob");
                    //valid
                    exist = qC.Number;
                } else {
                    Debug.Log("Exist Card PROB");
                    pass = false;
                    break;
                }
            }
            if(qC.Type == QuantityConcept.QuantityTypeEnum.INCREASE) {
                IncreaseProblem = true;
                if(i == 0 || i == QuantityConceptsOfFrames.Count-1) {
                    pass = false;
                    break;
                } else {
                    exist += qC.Number;
                }
            }
            if(qC.Type == QuantityConcept.QuantityTypeEnum.DECREASE) {
                if(i == 0 || i == QuantityConceptsOfFrames.Count-1) {
                    pass = false;
                    break;
                } else {
                    exist -= qC.Number;
                }
            }
        }
        }
        
        if(pass) {
            
            var animConfig = animationManager.AnimationConfiguration;
            animConfig.ClearConfig();

            for (int i = 0; i < dragAndDropManager.DropSpaceAmount; i++) {
                int frameIndex = dragAndDropManager.FrameIndexOnSpot(i);
                //put correct frame contents and quantity concepts on animconfig...

                var frameConts = framesManager.FrameContentDatas;
                animConfig.FrameContents.Add(frameConts[frameIndex]);
                animConfig.QuantityConcepts.Add(QuantityConceptsOfFrames[frameIndex]);
            }
            animationManager.Show();
            SoundSingleton.Instance.EndAll();
            SoundSingleton.Instance.PlaySound("yatta");

        } else {
            wrongAnimation.Invoke();
        }
    }

    internal void LockFrameInGoodPosition(int frameId, int spotId = -1) {
        if(spotId == -1) spotId = frameId;
        dragAndDropManager.LockDraggableOnSpot(frameId, spotId);
    }
}
