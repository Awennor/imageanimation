﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class FramesManager : MonoBehaviour {

    public ImageFrame[] imageFrames;
    public ObjectHolder objectHolder;
    public BackgroundHolder backgroundHolder;
    [SerializeField]
    private List<FrameContentData> frameContentDatas;
    bool infoOnStart = false;
    FrameContentToSound frameContentToSound = new FrameContentToSound();
    public Transform[] framePositions;
    List<Transform> aux = new List<Transform>();
    Dictionary<int,int> aux2 = new Dictionary<int, int>();

    public List<FrameContentData> FrameContentDatas {
        get {
            return frameContentDatas;
        }

        set {
            frameContentDatas = value;
        }
    }

    internal void ShowFrame(int v)
    {
        imageFrames[v].gameObject.SetActive(true);
    }

    internal void HideAllFrames()
    {
        for (int i = 0;  i< imageFrames.Length; i++)
        {
            imageFrames[i].gameObject.SetActive(false);
        }
    }


    // Use this for initialization
    void Start() {
        if (infoOnStart) {
            LoadInfo();
        }
    }

    public void RandomPositions() {
        for (int i = 0; i < imageFrames.Length; i++) {
            if (imageFrames[i].gameObject.activeSelf)
                aux.Add(framePositions[i]);
        }
        for (int i = 0; i < imageFrames.Length; i++) {
            if (imageFrames[i].gameObject.activeSelf) {
                int positionIndex = RandomSpecial.Range(0, aux.Count);
                imageFrames[i].transform.position = aux[positionIndex].position;
                aux.RemoveAt(positionIndex);
                imageFrames[i].transform.rotation = Quaternion.identity;
            }

            //framePositions
        }
    }

    internal void FramesActive(int v) {
        for (int i = 0; i < imageFrames.Length; i++) {
            imageFrames[i].gameObject.SetActive(i < v);
        }
    }

    public void LoadInfo() {
        for (int i = 0; i < FrameContentDatas.Count; i++) {
            var imageFrame = imageFrames[i];
            var frameContentData = FrameContentDatas[i];
            ContentToFrame(imageFrame, frameContentData);

        }
    }

    public void ContentToFrame(ImageFrame imageFrame, FrameContentData frameContentData) {
        var objects = frameContentData.ObjectsEnums;
        var bgEnum = frameContentData.BackgroundEnum;

        imageFrame.SetBackground(backgroundHolder.NewInstance((int)bgEnum));
        aux2.Clear();
        for (int j = 0; j < objects.Count; j++) {
            var obj = objectHolder.NewInstance(objects[j]);
            obj.RepresentationActive(frameContentData.RepresentationType[j]);

            int pos; 
            
            var groupId = frameContentData.ObjectsEnumGroupId[j];
            Debug.Log("CONTENT TO FRAME GROUP ID "+groupId);
            if(!aux2.ContainsKey(groupId)) {
                aux2[groupId] = 0;
            }
            pos = aux2[groupId];
            imageFrame.AddObjectToFrame(pos, obj, groupId);
            aux2[groupId]++;
            frameContentToSound.PutInSounds(imageFrame, frameContentData);
        }
    }

    internal void ObjectsMovingIn(ImageFrame imageFrame, FrameContentData frameContentData, int positionOffsetMovingIn) {
        var objects = frameContentData.ObjectsEnums;

        for (int j = 0; j < objects.Count; j++) {
            var obj = objectHolder.NewInstance(objects[j]);
            obj.RepresentationActive(frameContentData.RepresentationType[j]);
            imageFrame.AddObjectToFrameOutside(j + positionOffsetMovingIn, obj);
        }
    }

    internal void EnableDraggingAll() {
        for (int i = 0; i < imageFrames.Length; i++) {
            imageFrames[i].GetComponent<Draggable>().enabled = true;
        }
    }

    internal void StopDragging(int cardLocked) {
        imageFrames[cardLocked].GetComponent<Draggable>().enabled = false;
    }

    internal void StopDraggingAll()
    {
        for (int i = 0; i < imageFrames.Length; i++)
        {
            imageFrames[i].GetComponent<Draggable>().enabled = false;
        }
        
    }

    // Update is called once per frame
    void Update() {

    }
}
