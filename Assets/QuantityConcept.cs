﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[Serializable]
public class QuantityConcept {
    [SerializeField]
    List<int> numbers = new List<int>();
    [SerializeField]
    QuantityTypeEnum type;

    public int Number {
        get {
            if(Numbers.Count < 1) Numbers.Add(0);
            return Numbers[0];
        }

        set {
            if(Numbers.Count < 1) Numbers.Add(0);
            Numbers[0] = value;
        }
    }

    public List<int> Numbers {
        get {
            return numbers;
        }

        set {
            numbers = value;
        }
    }

    public QuantityTypeEnum Type {
        get {
            return type;
        }

        set {
            type = value;
        }
    }

    public enum QuantityTypeEnum{
        EXIST, INCREASE, DECREASE,
        COMBINE,
    }

    internal int SumOfNumbers() {
        int sum = 0;
        for (int i = 0; i < numbers.Count; i++) {
            sum += numbers[i];
        }
        return sum;
    }

    internal void NumberOfValues(int quantity) {
        while(numbers.Count < quantity) {
            numbers.Add(0);
        }
    }
}

