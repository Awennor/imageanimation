﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class FrameContentToSound {

    public void PutInSounds(ImageFrame imageFrame, FrameContentData frameContentData) {
        var seqAudioCall = imageFrame.SequentialAudioCaller;
        if (seqAudioCall == null) return;
        var obj = frameContentData.ObjectsEnums[0];
        var audios = seqAudioCall.Audios;
        audios.Clear();
        audios.Add("toriga");
        audios.Add(frameContentData.ObjectsEnums.Count + "tsu");
        if (frameContentData.BackgroundEnum == BackgroundsEnum.backgroundbird) {
            audios.Add("arimasu");
        }
        if (frameContentData.BackgroundEnum == BackgroundsEnum.backgroundbirdenter) {
            audios.Add("kimasu");
        }

        if (frameContentData.BackgroundEnum == BackgroundsEnum.backgroundbirdleave) {
            audios.Add("detteimasu");
        }



    }

}
