﻿using UnityEngine;
using System.Collections;

public class Draggable : MonoBehaviour {
    private Vector3 startPosition;
    private Vector3 startMouse;
	//[SerializeField]
	//private Transform transform;

    public void OnDrag() {
        if(!enabled) return;
        Vector3 mousePosition = Input.mousePosition;
        
        //mousePosition = Camera.main.WorldToScreenPoint(mousePosition);

        transform.position = mousePosition - startMouse + startPosition;
    }

    public void EndDrag() {
        
    }

    public void StartDrag() {
        if(!enabled) return;
        startPosition = transform.position;
        Vector3 mousePosition = Input.mousePosition;
        
        //mousePosition = Camera.main.WorldToScreenPoint(mousePosition);
        startMouse = mousePosition;
    }

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
