﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using System;
using System.Collections.Generic;

public class ConceptArrangerManager : MonoBehaviour {

	

    List<int> correctPositions = new List<int>();

	public DragAndDropManager dragDrop;

	public UnityEvent CorrectGuess;
	public UnityEvent BadGuess;

    public ConceptHolder conceptHolder;

	public int CorrectOne {
		get {
            if(correctPositions.Count == 0)
                correctPositions.Add(-1);
			return correctPositions[0];
		}
		set {
            if(correctPositions.Count == 0)
                correctPositions.Add(-1);
			correctPositions[0] = value;
		}
	}

	public void PressCheckButton(){

        var correctItem = (dragDrop.IsCorrect(correctPositions));
        //if(correctPositions)
        if (correctItem) {
			CorrectGuess.Invoke();
            //conceptHolder.gameObject.SetActive(false);
            
		} else {
			BadGuess.Invoke();
		}
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    internal void HideConcepts() {
        conceptHolder.gameObject.SetActive(false);
    }
}
