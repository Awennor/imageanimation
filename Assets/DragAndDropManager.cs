﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Events;
using System.Collections.Generic;

public class DragAndDropManager : MonoBehaviour {

    public Draggable[] draggables;
    public RectTransform[] spots;

	public UnityEvent AllSpotsFilled;
	public UnityEvent NotAllSpotsFilled;

    public float minDistance;

    IntVectorCustom draggableWhichSpot = new IntVectorCustom();

    public int DropSpaceAmount { get { return spots.Length; } }

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        for (int i = 0; i < draggables.Length; i++) {
			int oldSpotPos = draggableWhichSpot[i];
            var dragPos = draggables[i].transform.position;
            bool noLock = true;
            for (int j = 0; j < spots.Length; j++) {
                var spotPos = spots[j].transform.position;
                float mag = (spotPos - dragPos).sqrMagnitude;
                if (mag < minDistance * minDistance) {
                    noLock = false;
                    LockDraggableOnSpot(i,j);
                }
            }
            if (noLock) {
                draggableWhichSpot[i] = -1;
                draggables[i].transform.rotation = Quaternion.identity;
            }
			if(oldSpotPos != draggableWhichSpot[i]){
				TryCallback();
			}
        }

    }

    internal bool IsCorrect(List<int> correctPositions) {
        for (int i = 0; i < correctPositions.Count; i++) {
            if(correctPositions[i] != FrameIndexOnSpot(i)) {
                return false;
            }
        }
        return true;
        
    }

    void TryCallback ()
	{
		bool allFill = true;
		for (int i = 0; i < spots.Length; i++) {
			if(FrameIndexOnSpot(i) == -1){
				allFill= false;
			}
		}
		if (allFill == true) {
			AllSpotsFilled.Invoke ();
		} else {
			NotAllSpotsFilled.Invoke ();

		}
	}

    internal int FrameIndexOnSpot(int spotIndex) {
        for (int i = 0; i < draggableWhichSpot.Length; i++) {
            if (draggableWhichSpot[i] == spotIndex) {
                return i;
            }
        }
        return -1;
    }

    internal void LockDraggableOnSpot(int draggableId, int spotId) {
        int i = draggableId;
        int j = spotId;
        var spotPos = spots[j].transform.position;
        draggables[i].transform.position = spotPos;
        draggables[i].transform.rotation = spots[j].transform.rotation;
        draggableWhichSpot[i] = j;
    }
}
