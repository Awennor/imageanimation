﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class ImageFrame : MonoBehaviour {

    [SerializeField]
    FrameBackground frameBackground;

    Dictionary<int, ObjectGraphical>[] objectsInPosition = new Dictionary<int, ObjectGraphical>[3];
    Dictionary<int, ObjectGraphical> objectsOutside = new Dictionary<int, ObjectGraphical>();

    private SequentialAudioCaller sequentialAudioCaller;

    public SequentialAudioCaller SequentialAudioCaller {
        get {
            return sequentialAudioCaller;
        }

        set {
            sequentialAudioCaller = value;
        }
    }

    public Dictionary<int, ObjectGraphical>[] ObjectsInPosition {
        get {
            return objectsInPosition;
        }

        set {
            objectsInPosition = value;
        }
    }

    public Vector3 GetObjectPosition(int pos, int groupId) {
        return frameBackground.ObjectPositions[groupId][pos].transform.position;
    }

     public Vector3 GetObjectPositionOutside(int pos) {
        return frameBackground.OutsidePositions[pos].transform.position;
    }

    internal void AddObjectToFrame(int pos, ObjectGraphical obj) {
        const int groupId = 0;
        AddObjectToFrame(pos, obj, groupId);
    }

    public void AddObjectToFrame(int pos, ObjectGraphical obj, int groupId) {
        var position = GetObjectPosition(pos, groupId);
        obj.transform.position = position;
        obj.transform.SetParent(frameBackground.ObjectPositions[groupId][pos].transform);
        obj.transform.localScale = new Vector3(1, 1, 1);
        ObjectsInPosition[groupId][pos] = obj;
    }

    internal void AddObjectToFrameOutside(int pos, ObjectGraphical obj) {
        var position = GetObjectPositionOutside(pos);
        obj.transform.position = position;
        obj.transform.SetParent(frameBackground.OutsidePositions[pos].transform);
        obj.transform.SetParent(transform);
        obj.transform.localScale = new Vector3(1, 1, 1);
        objectsOutside[pos] = obj;
    }

    internal ObjectGraphical ObjectOutside(int index) {
        return objectsOutside[index];
    }

    internal ObjectGraphical ObjectInside(int index) {
        return ObjectsInPosition[0][index];
    }

    internal void SetBackground(FrameBackground frameBackground) {
        this.frameBackground = frameBackground;
        frameBackground.transform.SetParent(transform);
        frameBackground.transform.position = transform.position;
        frameBackground.transform.localScale = new Vector3(1, 1, 1);
    }

    void Awake() {
        for (int i = 0; i < ObjectsInPosition.Length; i++) {
            ObjectsInPosition[i] = new Dictionary<int, ObjectGraphical>();
        }
    }

    void OnEnable() {
        sequentialAudioCaller =  GetComponent<SequentialAudioCaller>();
        
    }

    

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    internal GameObject GetCustomObject(int v) {
        return frameBackground.ExtraObjects[v];
    }

    internal Vector3 GetCustomPosition(int v) {
        return frameBackground.ExtraPositions[v].transform.position;
    }
}
