﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ObjectHolder: MonoBehaviour {

    //Dictionary<string, GameObject> objects = new Dictionary<string, GameObject>();
    List<GameObject> objects = new List<GameObject>();

	// Use this for initialization
	void OnEnable () {
	    int cc = transform.childCount;
        for (int i = 0; i < cc; i++) {
            GameObject c = transform.GetChild(i).gameObject;
            objects.Add(c);
        }
	}

    internal ObjectGraphical NewInstance(int objId) {
        var obj = objects[objId];
        return Instantiate(obj).GetComponent<ObjectGraphical>();
    }

    internal ObjectGraphical NewInstance(Enum e) {
        int objId = Convert.ToInt32(e);
        return NewInstance(objId);
    }

    // Update is called once per frame  
    void Update () {
	
	}
}
