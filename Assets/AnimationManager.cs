using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.Events;

public class AnimationManager : MonoBehaviour {

    [SerializeField]
    AnimationConfiguration animationConfiguration;
    [SerializeField]
    FramesManager framesManager;
    public bool playOnStart = false;
    [SerializeField]
    private ImageFrame imageFrame;
    public GameObject graphics;
    public float timeToMove = 1.9f;
    public float timeToExist = 0.7f;
    public Button closeButton;
    public Button playButton;
    public UnityEvent AnimationClose;
    public List<Func<AnimationConfiguration, bool>> AnimationPlayCustom = new List<Func<AnimationConfiguration, bool>>();
    public List<Func<AnimationConfiguration, bool>> AnimationFirstFrameCustom = new List<Func<AnimationConfiguration, bool>>();
    //public List<MonoBehaviour> monoBehaviours;

    internal void ShowGraphics(bool b) {
        graphics.SetActive(b);
    }

    public AnimationConfiguration AnimationConfiguration {
        get {
            return animationConfiguration;
        }

        set {
            animationConfiguration = value;
        }
    }

    public ImageFrame ImageFrame {
        get {
            return imageFrame;
        }

        set {
            imageFrame = value;
        }
    }

    public FramesManager FramesManager {
        get {
            return framesManager;
        }

        set {
            framesManager = value;
        }
    }




    // Use this for initialization
    void Start () {
	    if(playOnStart) {
            Play();
        }
    }

    public void CloseButtonPress() {
        ShowGraphics(false);
        AnimationClose.Invoke();
    }

    public void SetupFirstFrame() {
        for (int i = 0; i < AnimationFirstFrameCustom.Count; i++) {
            bool interrupt = AnimationFirstFrameCustom[i](animationConfiguration);
            if(interrupt) return;
        }
        var frameContents = AnimationConfiguration.FrameContents;
        FramesManager.ContentToFrame(ImageFrame, frameContents[0]);
    }

    public void Play() {
        for (int i = 0; i < AnimationPlayCustom.Count; i++) {
            bool interrupt = AnimationPlayCustom[i](animationConfiguration);
            if(interrupt) return;
        }

        var frameContents = AnimationConfiguration.FrameContents;
        var qCs = AnimationConfiguration.QuantityConcepts;
        int objectsStationaryAtTime = 0;
        float timeOffset = 0f;
        for (int i = 0; i < qCs.Count; i++) {
            var qC = qCs[i];
            if (qC.Type == QuantityConcept.QuantityTypeEnum.EXIST) {
                objectsStationaryAtTime = qC.Number;
                timeOffset += timeToExist;
            }
            if (qC.Type == QuantityConcept.QuantityTypeEnum.INCREASE) {
                int positionOffsetMovingIn = objectsStationaryAtTime;
                FramesManager.ObjectsMovingIn(ImageFrame, frameContents[i], positionOffsetMovingIn);
                
                for (int j = 0; j < frameContents[i].ObjectsEnums.Count; j++) {
                    int index = positionOffsetMovingIn + j;
                    var objGr = ImageFrame.ObjectOutside(index);
                    var seq = DOTween.Sequence();
                    seq.AppendInterval(timeOffset);
                    
                    //Debug.Log(index + " INDEX");

                    seq.Append(objGr.transform.DOMove(ImageFrame.GetObjectPosition(index, 0), timeToMove));
                    seq.AppendCallback(objGr.StaticRepresentation);
                }
                timeOffset += timeToMove;
            }
            if (qC.Type == QuantityConcept.QuantityTypeEnum.DECREASE) {
                int positionOffsetMovingIn = objectsStationaryAtTime;
                //framesManager.ObjectsMovingIn(imageFrame, frameContents[i], positionOffsetMovingIn);
                
                for (int j = 0; j < frameContents[i].ObjectsEnums.Count; j++) {
                    int index = positionOffsetMovingIn - j-1;
                    var objGr = ImageFrame.ObjectInside(index);
                    var seq = DOTween.Sequence();
                    seq.AppendInterval(timeOffset);
                    
                    seq.AppendCallback(objGr.MoveOutRepresentation);
                    seq.Append(objGr.transform.DOMove(ImageFrame.GetObjectPositionOutside(index), timeToMove));
                    
                }
                timeOffset += timeToMove;
            }
        }
        DOVirtual.DelayedCall(timeOffset, AnimationOver);
    }

    private bool someMethod(AnimationConfiguration arg) {
        throw new NotImplementedException();
    }

    public void AnimationOver() {
        closeButton.gameObject.SetActive(true);
    }

    internal void Show() {
        playButton.gameObject.SetActive(true);
        closeButton.gameObject.SetActive(false);
        ShowGraphics(true);
        SetupFirstFrame();
        //Play();
    }

    // Update is called once per frame
    void Update () {
	
	}
}

[Serializable]
public class AnimationConfiguration{
    [SerializeField]
    List<FrameContentData> frameContents = new List<FrameContentData>();
    [SerializeField]
    List<QuantityConcept> quantityConcepts = new List<QuantityConcept>();

    public void ClearConfig() {
        frameContents.Clear();
        quantityConcepts.Clear();
    }

    public List<FrameContentData> FrameContents {
        get {
            return frameContents;
        }

        set {
            frameContents = value;
        }
    }

    public List<QuantityConcept> QuantityConcepts {
        get {
            return quantityConcepts;
        }

        set {
            quantityConcepts = value;
        }
    }
}