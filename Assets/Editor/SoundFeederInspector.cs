﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(SoundFeeder))]
class SoundFeederInspector : Editor {

    public override void OnInspectorGUI() {
        DrawDefaultInspector();
        if (GUILayout.Button("Generate game objects with sounds")) {
            SoundFeeder myTarget = (SoundFeeder)target;
            var audios = myTarget.audioSourceForAutomation;
            for (int i = 0; i < audios.Length; i++) {
                string name = audios[i].name;
                if(myTarget.transform.Find(name) != null) {
                    continue;
                }
                var obj = new GameObject();
                obj.name = name;
                obj.transform.SetParent(myTarget.transform);
                var aS = obj.AddComponent<AudioSource>();
                aS.clip = audios[i];
                aS.playOnAwake = false;

            }
        }
            
    }

}
