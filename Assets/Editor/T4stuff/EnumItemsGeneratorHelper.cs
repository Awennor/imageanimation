﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Editor.T4stuff {
    partial class EnumItemsGenerator {
        private string className;

        /// <summary>
        /// A datasource for which concrete static fields will be created on the generated class.
        /// </summary>
        private string[] source;

        public EnumItemsGenerator(string generatedClassName, string[] source) {


            this.className = generatedClassName;
            this.source = source;
        }
    }
}

