﻿using UnityEngine;
using System.Collections;
using UnityEditor;

using UnityEngine;
using System.Collections;
using UnityEditor;
using CodeGeneration;
using Assets.Editor.T4stuff;
using System.IO;
using System;
using System.Text;

[CustomEditor(typeof(ObjectHolder))]
public class ObjectHolderInspector : Editor {

    //static private TreasureIdManager treasureIdManager = new TreasureIdManager();
    public override void OnInspectorGUI() {
        DrawDefaultInspector();
        ObjectHolder myTarget = (ObjectHolder)target;



        if (GUILayout.Button("Generate Enum")) {
            //Debug.Log("Try generate");
            const string name = "ObjectsEnum";
            int length = myTarget.transform.childCount;
            string[] sourceKeys = new string[length];
            for (int i = 0; i < length; i++) {
                sourceKeys[i] = myTarget.transform.GetChild(i).name;
            }
            var generator = new EnumItemsGenerator(name, sourceKeys);

            var classDefintion = generator.TransformText();
            Debug.Log(classDefintion);

            var outputPath = Path.Combine(Application.dataPath + "/Scripts/", name + ".cs");

            try {
                // Save new class to assets folder.
                File.WriteAllText(outputPath, classDefintion);
                //Debug.Log("Generated!" + outputPath);

                // Refresh assets.
                AssetDatabase.Refresh();
            } catch (Exception e) {
                //Debug.Log("An error occurred while saving file: " + e);
            }
        }

    }

    void Start() {

    }

    void Update() {

    }
}
