﻿using UnityEngine;
using System.Collections;
using System;

public class CombinationAnimationVerification : MonoBehaviour {
    private CardToAnimationManager animationManager;

    // Use this for initialization
    void Start() {
        animationManager = GetComponent<CardToAnimationManager>();
        animationManager.AnimationVerificationCustom.Add(Verificate);
    }

    private bool Verificate() {
        bool pass = true;
        var QuantityConceptsOfFrames = animationManager.QuantityConceptsOfFrames;
        var dragAndDropManager = animationManager.dragAndDropManager;
        int value1 = -1;
        int value2 = -1;
        int value1Comb = -1;
        int value2Comb = -1;
        for (int i = 0; i < dragAndDropManager.DropSpaceAmount; i++) {
            int frameIndex = dragAndDropManager.FrameIndexOnSpot(i);
            if (frameIndex == -1) {
                pass = false;
                break;
            }
            var qC = QuantityConceptsOfFrames[frameIndex];
            if (qC.Type == QuantityConcept.QuantityTypeEnum.EXIST) {
                Debug.Log("Exist Card");
                if (value1 == -1) {

                    value1 = qC.Number;
                } else {
                    if (value2 == -1) {
                        value2 = qC.Number;
                    }
                }
            }
            if(((i == 0)|| (i==1)) && qC.Type == QuantityConcept.QuantityTypeEnum.COMBINE) {
                return false;
            }
            if(((i == 2)) && qC.Type != QuantityConcept.QuantityTypeEnum.COMBINE) {
                return false;
            }
            if (qC.Type == QuantityConcept.QuantityTypeEnum.COMBINE) {
                Debug.Log("Exist Card");
                value1Comb = qC.Numbers[0];
                value2Comb = qC.Numbers[1];
            }
            if (qC.Type == QuantityConcept.QuantityTypeEnum.INCREASE) {
                return false;
            }
            if (qC.Type == QuantityConcept.QuantityTypeEnum.DECREASE) {
                return false;
            }



        }
        if((value1 == value1Comb && value2 == value2Comb) ||
            (value1 == value2Comb && value2 == value1Comb)
            ) {
            return true;
        }
        return false;
    }

    // Update is called once per frame
    void Update() {

    }
}
