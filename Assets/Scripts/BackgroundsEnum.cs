
/// <summary>
/// Generates a safe wrapper for BackgroundsEnum.
/// </summary>
public enum BackgroundsEnum
{
    backgroundbird,
    backgroundbirdleave,
    backgroundbirdenter,
    backgroundtableboy,
    backgroundtablegirl,
    backgroundtableboth,
    backgroundtablebothanim,
}