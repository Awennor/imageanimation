﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SequentialAudioCaller : MonoBehaviour {

    [SerializeField]
    List<string> audios = new List<string>();
    public bool play = true;

    public List<string> Audios {
        get {
            return audios;
        }

        set {
            audios = value;
        }
    }

    public void Play() {
        if(play)
            SoundSingleton.Instance.SequentialAudioPlayer.Play(Audios);
    }
}
