﻿using UnityEngine;
using System.Collections;

public class SoundCaller : MonoBehaviour {

    public string defaultSound;
    public bool playOnEnable;
    public bool pauseAllOnPlay;

    public void Play(string soundName) {
        SoundSingleton.Instance.EndAll();
        SoundSingleton.Instance.PlaySound(soundName);
    }

    public void OnEnable() {
        if(playOnEnable)
            Play(defaultSound);
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
