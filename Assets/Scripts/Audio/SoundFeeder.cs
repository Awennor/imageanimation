﻿using UnityEngine;
using System.Collections;

public class SoundFeeder : MonoBehaviour {

    public AudioClip[] audioSourceForAutomation;

    void Awake() {
        if(!SoundSingleton.Instance.hasAny()) {
            int childCount = transform.childCount;
            for (int i = 0; i < childCount; i++) {
                var obj = transform.GetChild(i).gameObject;
                string n = obj.name;
                SoundSingleton.Instance.AddSound(n, obj.GetComponent<AudioSource>());
            }
        }
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
