﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Assets.ReusablePidroh.Scripts;

public class SoundSingleton{

    static SoundSingleton instance;
    private GameObject gameObject;
    private Invoker invoker;
    Dictionary<string, AudioSource> sounds = new Dictionary<string, AudioSource>();
    SequentialAudioPlayer sequentialAudioPlayer = new SequentialAudioPlayer();

    public static SoundSingleton Instance {
        get {
            if(instance == null) {
                instance = new SoundSingleton();
                var gameObject = new GameObject();
                gameObject.name = "SoundAuxiliar";
                GameObject.DontDestroyOnLoad(gameObject);
                instance.invoker = gameObject.AddComponent<Invoker>();
            }
            return instance;
        }

        set {
            instance = value;
        }
    }

    internal void EndAll() {
        var keys = sounds.Keys;
        var enumerator = keys.GetEnumerator();
        for (int i = 0; i < keys.Count; i++) {
            var current = enumerator.Current;
            if(current == null) break;
            var s = sounds[current];
            s.Stop();
            bool hasNext = enumerator.MoveNext();
            if(!hasNext) break;
        }
        sequentialAudioPlayer.StopSequence();
    }

    public SequentialAudioPlayer SequentialAudioPlayer {
        get {
            return sequentialAudioPlayer;
        }

        set {
            sequentialAudioPlayer = value;
        }
    }

    public void AddSound(string key, AudioSource aS) {
        sounds.Add(key, aS);
    }

    public bool hasAny() {
        return sounds.Count > 0;
    }

    internal void StopSoundDelay(string sound, float delay) {
        invoker.AddAction(()=> {StopSound(sound); }, delay);

    }

    private void StopSound(string v) {
         if(sounds.ContainsKey(v)) {
            sounds[v].Stop();
            
        } 
    }

    internal void PlaySound(string v, float delay) {
        
        if(sounds.ContainsKey(v)) {
            invoker.AddAction(()=> {sounds[v].Play(); }, delay);    
            
        } else {
            Debug.Log("SOUND NOT FOUND!!! key is "+v);
        }
        
    }

    internal void PlaySoundOnPause(string v) {
        if(sounds.ContainsKey(v)) {
            sounds[v].ignoreListenerPause = true;
            sounds[v].Play();
            //sounds[v].ignoreListenerPause = false;
            
        } else {
            Debug.Log("SOUND NOT FOUND!!! key is "+v);
        }
        
    }

    internal AudioSource PlaySound(string v) {
        if(sounds.ContainsKey(v)) {
            var audioSource = sounds[v];
            audioSource.Play();
            return audioSource;
            
        } else {
            Debug.Log("SOUND NOT FOUND!!! key is "+v);
            return null;
        }
        
    }
}
