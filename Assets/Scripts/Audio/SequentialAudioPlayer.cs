﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using System;

public class SequentialAudioPlayer {

    List<string> audioList = new List<string>();
    private bool noConcurrency = true;
    private Tween tween;

    public void Play() {
        if(audioList.Count > 0) {
            var aS = SoundSingleton.Instance.PlaySound(audioList[0]);
            float timeToFinishPlay = aS.clip.length;
            tween = DOVirtual.DelayedCall(timeToFinishPlay, Play);
            audioList.RemoveAt(0);
        }
            
    }

    internal void Play(List<string> audios) {
        if(tween != null && noConcurrency) {
            tween.Kill();
        }
        SoundSingleton.Instance.EndAll();
        audioList.Clear();
        audioList.AddRange(audios);
        Play();
    }

    internal void StopSequence() {
        if(tween != null && noConcurrency) {
            tween.Kill();
        }
    }
}
