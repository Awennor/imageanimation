﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

[Serializable]
public class AutomaticPositioner {
	[SerializeField]
	List<GameObject> positions;
	[SerializeField]
	List<GameObject> objects;

    List<GameObject> positionsAux = new List<GameObject>();

    public List<GameObject> Objects
    {
        get
        {
            return objects;
        }

        set
        {
            objects = value;
        }
    }

    internal void RandomReposition()
    {
        Debug.Log("RANDOM POSITION!!!");
        positionsAux.Clear();
        for (int i = 0; i < Objects.Count; i++)
        {
            positionsAux.Add(positions[i]); 
        }
        for (int i = 0; i < Objects.Count; i++)
        {
            Debug.Log("RANDOM POSITION!!!" + i);
            Objects[i].transform.position = positionsAux.RemoveRandom().transform.position;

        }
    }
}
