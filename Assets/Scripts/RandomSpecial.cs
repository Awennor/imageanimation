﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class RandomSpecial
    {

    public static bool RandomBool() {
        return RandomBool(0.5f);

    }

    internal static float randomFloatOnZero(float v)
    {
        return UnityEngine.Random.Range(-v, v);
    }

    internal static float randomFloat(float max) {
        return UnityEngine.Random.Range(0, max);
    }

    internal static bool RandomBool(float v) {
        return UnityEngine.Random.Range(0f, 1f) < v;
    }

    internal static float Range(float v1, float v2) {
        return UnityEngine.Random.Range(v1, v2);
    }

    internal static int Range(int v1, int v2) {
        return UnityEngine.Random.Range(v1, v2);
    }
}


