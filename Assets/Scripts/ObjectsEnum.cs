
/// <summary>
/// Generates a safe wrapper for ObjectsEnum.
/// </summary>
public enum ObjectsEnum
{
    bird,
    apple,
}