﻿using UnityEngine;
using System.Collections;

public class ObjectGraphical : MonoBehaviour {

    public GameObject[] representations;

    public void RepresentationActive(ObjectGraphicalRepresentations representation){
        for (int i = 0; i < representations.Length; i++) {
            representations[i].SetActive(i == (int) representation);
        }
    }

    public void StaticRepresentation() {
        RepresentationActive(ObjectGraphicalRepresentations.STATIC);
    }

    public void MoveInRepresentation() {
        RepresentationActive(ObjectGraphicalRepresentations.MOVEOUT);
		Debug.Log ("MOVEIN");
    }

	public void MoveOutRepresentation() {
		RepresentationActive(ObjectGraphicalRepresentations.MOVEOUT);
		Debug.Log ("MOVEOUT");
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

public enum ObjectGraphicalRepresentations{
    STATIC, MOVEIN, MOVEOUT,
}