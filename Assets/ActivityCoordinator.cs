﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ActivityCoordinator : MonoBehaviour {

    public FramesManager framesManager;
    public CardToAnimationManager cardToAnimationArranger;
    public ConceptArrangerManager conceptArrangerManager;
    


    public bool AskStoryTypeAfterAnimation;
    //public bool AskPhraseConfigurationAfterAnimation;

    int problemLevel = 0;
    List<int> staticValuesAux = new List<int>();

    public void AnimationOver() {
        framesManager.StopDraggingAll();
        cardToAnimationArranger.HideUnused();

        if (AskStoryTypeAfterAnimation) {
            bool increaseProblem = cardToAnimationArranger.IncreaseProblem;
            if (increaseProblem) {
                conceptArrangerManager.CorrectOne = 0;
            } else {

                conceptArrangerManager.CorrectOne = 1;
            }
        } else {
            
            
            
        }

        
        

    }

    public void NewProblem() {
        //CombinationProblem();
        IncreaseDecreaseProblemLine();
    }

    private void CombinationProblem() {
        conceptArrangerManager.HideConcepts();
        problemLevel++;
        problemLevel = 400;
        int maxSum = 3;
        maxSum = 3 + problemLevel / 2;
        if (maxSum > 9) maxSum = 9;
        int minSum = maxSum - 1;

        int sum = RandomSpecial.Range(minSum, maxSum + 1);

        int maxValueBoy = sum - 1;
        if (maxValueBoy > 7) maxValueBoy = 7;

        int valueBoy = RandomSpecial.Range(1, maxValueBoy + 1);

        int dummyCards = 0;
        int cardsLocked = 0;



        if (false
            || problemLevel == 2
            || problemLevel == 4
            || problemLevel > 6
            ) {
            dummyCards = 1;
        }
        if (problemLevel > 9) {
            dummyCards = 2;
        }

        if (problemLevel > 13) {
            dummyCards = 3;
        }



        if (problemLevel == 4) {
            dummyCards = 1;
        }
        if (problemLevel < 3) {
            cardsLocked = 2;
        } else {
            if (problemLevel < 5) {
                cardsLocked = 1;
            }
        }

        framesManager.FramesActive(3 + dummyCards);

        framesManager.RandomPositions();

        var framesContent = framesManager.FrameContentDatas;
        var quantityConcepts = cardToAnimationArranger.QuantityConceptsOfFrames;



        int valueGirl = sum - valueBoy;
        quantityConcepts[0].Number = valueBoy;
        quantityConcepts[0].Type = QuantityConcept.QuantityTypeEnum.EXIST;

        //quantityConcepts[0]. = QuantityConcept.QuantityTypeEnum.EXIST;
        quantityConcepts[1].Number = valueGirl;
        quantityConcepts[1].Type = QuantityConcept.QuantityTypeEnum.EXIST;

        quantityConcepts[2].NumberOfValues(2);
        quantityConcepts[2].Numbers[0] = valueBoy;
        quantityConcepts[2].Numbers[1] = valueGirl;
        quantityConcepts[2].Type = QuantityConcept.QuantityTypeEnum.COMBINE;

        framesContent[0].BackgroundEnum = BackgroundsEnum.backgroundtableboy;


        var objectShown = ObjectsEnum.apple;

        framesContent[0].FillObjects(objectShown, valueBoy, 0);

        framesContent[1].BackgroundEnum = BackgroundsEnum.backgroundtablegirl;
        framesContent[1].FillObjects(objectShown, valueGirl);
        framesContent[2].BackgroundEnum = BackgroundsEnum.backgroundtableboth;
        framesContent[2].FillObjects(objectShown, valueBoy + valueGirl, 0);
        //framesContent[2].AddObjects(objectShown, valueGirl, 1);

        for (int i = 0; i < dummyCards; i++) {
            int index = i + 3;
            int cloneIndex = RandomSpecial.Range(0, 3);

            FillUpAuxInts(7);
            staticValuesAux.Remove(framesContent[cloneIndex].ObjectsEnums.Count);
            framesContent[index].BackgroundEnum = framesContent[cloneIndex].BackgroundEnum;
            int amountOfObjects = staticValuesAux.RandomItem();
            framesContent[index].FillObjects(objectShown, amountOfObjects);
        }




        framesManager.LoadInfo();
        framesManager.EnableDraggingAll();
        LockCards(cardsLocked);
    }

    private void IncreaseDecreaseProblemLine() {
        conceptArrangerManager.HideConcepts();
        problemLevel++;
        problemLevel += 400;
        int maxValue = 7;
        int cardsLocked = 0;
        int dummyCards = 0;

        maxValue = problemLevel / 3 + 3;
        if (maxValue > 7)
            maxValue = 7;
        FillUpAuxInts(maxValue);
        //staticValuesAux.Shuffle();
        if (false
            || problemLevel == 2
            || problemLevel == 4
            || problemLevel > 6
            ) {
            dummyCards = 1;
        }
        if (problemLevel > 9) {
            dummyCards = 2;
        }

        if (problemLevel > 13) {
            dummyCards = 3;
        }



        if (problemLevel == 4) {
            dummyCards = 1;
        }
        if (problemLevel < 3) {
            cardsLocked = 2;
        } else {
            if (problemLevel < 5) {
                cardsLocked = 1;
            }
        }


        int startValue = staticValuesAux.RemoveRandom();
        int endValue = staticValuesAux.RemoveRandom();

        framesManager.FramesActive(3 + dummyCards);

        framesManager.RandomPositions();

        var framesContent = framesManager.FrameContentDatas;
        var quantityConcepts = cardToAnimationArranger.QuantityConceptsOfFrames;
        var diff = startValue - endValue;
        bool increase = false;
        if (diff < 0) {
            increase = true;
            diff *= -1;
        }



        if (increase) {
            framesContent[1].BackgroundEnum = BackgroundsEnum.backgroundbirdenter;
        } else {
            framesContent[1].BackgroundEnum = BackgroundsEnum.backgroundbirdleave;
        }

        quantityConcepts[0].Number = startValue;
        quantityConcepts[0].Type = QuantityConcept.QuantityTypeEnum.EXIST;
        //quantityConcepts[0]. = QuantityConcept.QuantityTypeEnum.EXIST;
        quantityConcepts[1].Number = diff;
        if (increase)
            quantityConcepts[1].Type = QuantityConcept.QuantityTypeEnum.INCREASE;
        else
            quantityConcepts[1].Type = QuantityConcept.QuantityTypeEnum.DECREASE;
        quantityConcepts[2].Number = endValue;
        quantityConcepts[2].Type = QuantityConcept.QuantityTypeEnum.EXIST;

        var moveRepr = ObjectGraphicalRepresentations.MOVEIN;
        if (!increase) {
            moveRepr = ObjectGraphicalRepresentations.MOVEOUT;
        }

        framesContent[0].BackgroundEnum = BackgroundsEnum.backgroundbird;

        framesContent[0].FillObjects(ObjectsEnum.bird, startValue, 0);
        //framesContent[0].AddObjects(ObjectsEnum.bird, startValue, 1);
        framesContent[1].FillObjects(ObjectsEnum.bird, diff, 0, moveRepr);
        //framesContent[1].FillRepresentations(moveRepr, diff);
        framesContent[2].FillObjects(ObjectsEnum.bird, endValue);

        if (dummyCards > 0) {
            for (int i = 0; i < dummyCards; i++) {
                var dummyIndex = 3 + i;
                var valueOfDummy = staticValuesAux.RemoveRandom();
                framesContent[dummyIndex].FillObjects(ObjectsEnum.bird, valueOfDummy);
                //framesContent[dummyIndex].FillRepresentations(ObjectGraphicalRepresentations.STATIC, valueOfDummy);
                quantityConcepts[dummyIndex].Number = valueOfDummy;
                quantityConcepts[dummyIndex].Type = QuantityConcept.QuantityTypeEnum.EXIST;
            }
        }


        framesManager.LoadInfo();
        framesManager.EnableDraggingAll();
        LockCards(cardsLocked);
    }

    private void FillUpAuxInts(int maxValue) {
        staticValuesAux.Clear();
        for (int i = 1; i <= maxValue; i++) {
            staticValuesAux.Add(i);
        }
    }

    private void LockCards(int cardsLocked) {
        if (cardsLocked == 1) {
            var cardLocked = RandomSpecial.Range(0, 2);
            cardToAnimationArranger.LockFrameInGoodPosition(cardLocked);
            framesManager.StopDragging(cardLocked);
        }

        if (cardsLocked == 2) {
            var cardNotLocked = RandomSpecial.Range(0, 2);
            for (int i = 0; i < 3; i++) {
                if (i != cardNotLocked) {
                    cardToAnimationArranger.LockFrameInGoodPosition(i);
                    framesManager.StopDragging(i);
                }

            }
        }
    }

    // Use this for initialization
    void Start() {
        NewProblem();
    }

    // Update is called once per frame
    void Update() {

    }
}
